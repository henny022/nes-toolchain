grammar ASM;

asmfile: line* EOF;

line: directive | label | instruction;

directive: extern | d_global | section | data | address;

extern: EXTERN identifier;
d_global: GLOBAL identifier;
section: SECTION name=identifier;
data: DATA NUMBER (COMMA NUMBER)*;
address: address_number | address_identifier;
address_number: ADDRESS NUMBER;
address_identifier: ADDRESS identifier;

label: identifier COLON;

identifier: WORD | DATA;

num_ident: NUMBER | identifier;

immediate: HASH NUMBER;
accumulator: 'A';
zero_page: num_ident;
zero_page_x: num_ident COMMA 'X';
zero_page_y: num_ident COMMA 'Y';
absolute: num_ident;
absolute_x: num_ident COMMA 'X';
absolute_y: num_ident COMMA 'Y';
indexed_indirect: '(' num_ident COMMA 'X' ')';
indirect_indexed: '(' num_ident ')' COMMA 'Y';
indirect: '(' num_ident ')';

instruction: adc | op_and | asl | bcc | bcs | beq | bit | bmi | bne | bpl | brk | bvc | bvs | clc | cld | cli | clv | cmp |
                cpx | cpy | dec | dex | dey | eor | inc | inx | iny | jmp | jsr | lda | ldx | ldy | lsr | nop | ora |
                pha | php | pla | plp | rol | ror | rti | rts | sbc | sec | sed | sei | sta | stx | sty | tax | tay |
                tsx | txa | txs | tya;

izzaaaii: immediate | zero_page | zero_page_x | absolute | absolute_x | absolute_y | indexed_indirect | indirect_indexed;
zzaaaii: zero_page | zero_page_x | absolute | absolute_x | absolute_y | indexed_indirect | indirect_indexed;
azzaa: accumulator | zero_page | zero_page_x | absolute | absolute_x;
za: zero_page | absolute;
iza: immediate | zero_page | absolute;
zzaa: zero_page | zero_page_x | absolute | absolute_x;
ai: absolute | indirect;
izzaay: immediate | zero_page | zero_page_y | absolute | absolute_y;
izzaax: immediate | zero_page | zero_page_x | absolute | absolute_x;
zzay: zero_page | zero_page_y | absolute;
zzax: zero_page | zero_page_x | absolute;

adc: ADC operand=izzaaaii;
op_and: AND operand=izzaaaii;
asl: ASL operand=azzaa;
bit: BIT operand=za;
bcc: BCC operand=num_ident;
bcs: BCS operand=num_ident;
beq: BEQ operand=num_ident;
bmi: BMI operand=num_ident;
bne: BNE operand=num_ident;
bpl: BPL operand=num_ident;
bvc: BVC operand=num_ident;
bvs: BVS operand=num_ident;
brk: BRK;
cmp: CMP operand=izzaaaii;
cpx: CPX operand=iza;
cpy: CPY operand=iza;
dec: DEC operand=zzaa;
eor: EOR operand=izzaaaii;
clc: CLC;
sec: SEC;
cli: CLI;
sei: SEI;
clv: CLV;
cld: CLD;
sed: SED;
inc: INC operand=zzaa;
jmp: JMP operand=ai;
jsr: JSR operand=absolute;
lda: LDA operand=izzaaaii;
ldx: LDX operand=izzaay;
ldy: LDY operand=izzaax;
lsr: LSR operand=azzaa;
nop: NOP;
ora: ORA operand=izzaaaii;
tax: TAX;
txa: TXA;
dex: DEX;
inx: INX;
tay: TAY;
tya: TYA;
dey: DEY;
iny: INY;
rol: ROL operand=azzaa;
ror: ROR operand=azzaa;
rti: RTI;
rts: RTS;
sbc: SBC operand=izzaaaii;
sta: STA operand=zzaaaii;
txs: TXS;
tsx: TSX;
pha: PHA;
pla: PLA;
php: PHP;
plp: PLP;
stx: STX operand=zzay;
sty: STY operand=zzax;


HASH: '#';
COLON: ':';
COMMA: ',';

ADC: 'ADC' | 'adc';
AND: 'AND' | 'and';
ASL: 'ASL' | 'asl';
BCC: 'BCC' | 'bcc';
BCS: 'BCS' | 'bcs';
BEQ: 'BEQ' | 'beq';
BIT: 'BIT' | 'bit';
BMI: 'BMI' | 'bmi';
BNE: 'BNE' | 'bne';
BPL: 'BPL' | 'bpl';
BRK: 'BRK' | 'brk';
BVC: 'BVC' | 'bvc';
BVS: 'BVS' | 'bvs';
CLC: 'CLC' | 'clc';
CLD: 'CLD' | 'cld';
CLI: 'CLI' | 'cli';
CLV: 'CLV' | 'clv';
CMP: 'CMP' | 'cmp';
CPX: 'CPX' | 'cpx';
CPY: 'CPY' | 'cpy';
DEC: 'DEC' | 'dec';
DEX: 'DEX' | 'dex';
DEY: 'DEY' | 'dey';
EOR: 'EOR' | 'eor';
INC: 'INC' | 'inc';
INX: 'INX' | 'inx';
INY: 'INY' | 'iny';
JMP: 'JMP' | 'jmp';
JSR: 'JSR' | 'jsr';
LDA: 'LDA' | 'lda';
LDX: 'LDX' | 'ldx';
LDY: 'LDY' | 'ldy';
LSR: 'LSR' | 'lsr';
NOP: 'NOP' | 'nop';
ORA: 'ORA' | 'ora';
PHA: 'PHA' | 'pha';
PHP: 'PHP' | 'php';
PLA: 'PLA' | 'pla';
PLP: 'PLP' | 'plp';
ROL: 'ROL' | 'rol';
ROR: 'ROR' | 'ror';
RTI: 'RTI' | 'rti';
RTS: 'RTS' | 'rts';
SBC: 'SBC' | 'sbc';
SEC: 'SEC' | 'sec';
SED: 'SED' | 'sed';
SEI: 'SEI' | 'sei';
STA: 'STA' | 'sta';
STX: 'STX' | 'stx';
STY: 'STY' | 'sty';
TAX: 'TAX' | 'tax';
TAY: 'TAY' | 'tay';
TSX: 'TSX' | 'tsx';
TXA: 'TXA' | 'txa';
TXS: 'TXS' | 'txs';
TYA: 'TYA' | 'tya';

EXTERN: '.extern';
GLOBAL: '.global';
SECTION: '.section';
DATA: '.data';
ADDRESS: '.address';

NUMBER: '-'? ('0x' | '$')? [0-9a-fA-F]+;
WORD: [A-Za-z._][A-Za-z0-9._-]*;

COMMENT: '//' .*? (NL|EOF) -> skip;
WS: (' ' | '\t') -> skip;
NL: ('\r'|'\r'?'\n') -> skip;
