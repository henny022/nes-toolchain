# NES Toolchain

A programming Toolchain for the NES (6502) written from scratch in python

## what will this include
- assembler
- linker
- some objdump thing
- disassembler

This will use custom, selfmade file formats.

In the future offer integration with the x86to6502 by Jason Turner.

Maybe a custom compiler for a simple custom language.

## why python
because its simple and easy.

Maybe port it to c++ in the future.

## just why?
because why

also I want to see how far I can get with what I know