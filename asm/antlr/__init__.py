import antlr4.CommonTokenStream
import antlr4.InputStream

from .asm.ASMLexer import ASMLexer
from .asm.ASMListener import ASMListener
from .asm.ASMParser import ASMParser
from .asm.ASMVisitor import ASMVisitor


def parse(text: str) -> (ASMParser.AsmfileContext, bool):
    input = antlr4.InputStream(text)
    lexer = ASMLexer(input)
    tokens = antlr4.CommonTokenStream(lexer)
    parser = ASMParser(tokens)
    file = parser.asmfile()
    was_successful = parser.getNumberOfSyntaxErrors() == 0
    return file, was_successful
