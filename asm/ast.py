from typing import List, Union


class AstNode:
    pass


class AsmFile(AstNode):
    lines: List[AstNode]


# directives
class ExternDirective(AstNode):
    identifier: str


class GlobalDirective(AstNode):
    identifier: str


class SectionDirective(AstNode):
    section_name: str


class DataDirective(AstNode):
    data: List[int]


class AddressDirective(AstNode):
    target: Union[str, int]


class Label(AstNode):
    target: str


# addressing modes
class Operand(AstNode):
    pass


class Immediate(Operand):
    value: int


class Accumulator(Operand):
    pass


class ZeroPage(Operand):
    address: Union[str, int]
